acpi
alacritty
alsa-plugins
alsa-tools
alsa-utils
archcraft-about
archcraft-artworks
archcraft-backgrounds
archcraft-backgrounds-branding
archcraft-bspwm
archcraft-cursor-fluent
archcraft-cursor-future
archcraft-cursor-layan
archcraft-cursor-lyra
archcraft-cursor-material
archcraft-cursor-pear
archcraft-cursor-qogirr
archcraft-cursor-sweet
archcraft-cursor-vimix
archcraft-fonts
archcraft-grub-theme
archcraft-gtk-theme-adapta
archcraft-gtk-theme-arc
archcraft-gtk-theme-blade
archcraft-gtk-theme-cyberpunk
archcraft-gtk-theme-dracula
archcraft-gtk-theme-groot
archcraft-gtk-theme-gruvbox
archcraft-gtk-theme-hack
archcraft-gtk-theme-juno
archcraft-gtk-theme-kripton
archcraft-gtk-theme-manhattan
archcraft-gtk-theme-nordic
archcraft-gtk-theme-rick
archcraft-gtk-theme-spark
archcraft-gtk-theme-sweet
archcraft-gtk-theme-wave
archcraft-gtk-theme-white
archcraft-gtk-theme-windows
archcraft-help
archcraft-hooks
archcraft-icons-arc
archcraft-icons-breeze
archcraft-icons-hack
archcraft-icons-luna
archcraft-icons-luv
archcraft-icons-nordic
archcraft-icons-numix
archcraft-icons-papirus
archcraft-icons-qogir
archcraft-icons-white
archcraft-icons-win11
archcraft-icons-zafiro
archcraft-mirrorlist
archcraft-music
archcraft-nvim
archcraft-omz
archcraft-openbox
archcraft-pixmaps
archcraft-plymouth-theme
archcraft-scripts
archcraft-sddm-theme-default
archcraft-skeleton
archcraft-vim
atril
autoconf
automake
b43-fwcutter
baobab
base
bashtop
betterlockscreen
bind
binutils
bison
blueman
bluez
bluez-utils
bmon
brltty
broadcom-wl
bspwm
btrfs-progs
calc
cava
chaotic-keyring
chaotic-mirrorlist
clonezilla
cloud-init
colorpicker
crda
cryptsetup
cups
dhclient
dhcpcd
dialog
diffutils
dmraid
dnsmasq
dosfstools
downgrade
dunst
e2fsprogs
edk2-shell
efibootmgr
espeakup
ethtool
exfatprogs
f2fs-tools
fakeroot
fatresize
feh
ffmpeg
ffmpegthumbnailer
firefox
fortune-mod
fsarchiver
gcc
geany
geany-plugins
gnu-netcat
gpart
gparted
gpick
gpm
gptfdisk
grub
gst-libav
gst-plugins-bad
gst-plugins-base
gst-plugins-good
gst-plugins-ugly
gtk-engine-murrine
gutenprint
gvfs
gvfs-afc
gvfs-google
gvfs-gphoto2
gvfs-mtp
gvfs-smb
hdparm
highlight
htop
i3lock-color
imagemagick
inetutils
inotify-tools
intel-ucode
ipw2100-fw
ipw2200-fw
iw
iwd
jfsutils
jq
ksuperkey
kvantum
leafpad
less
libfido2
libusb-compat
light
linux
linux-atm
linux-firmware
livecd-sounds
lsb-release
lsscsi
lvm2
lxappearance
maim
make
man-db
man-pages
mdadm
meld
mkinitcpio
mkinitcpio-nfs-utils
modemmanager
mpc
mpd
mplayer
mtools
nano
nbd
ncdu
ncmpcpp
ndisc6
neovim
nethogs
networkmanager
networkmanager-dmenu-git
networkmanager-openvpn
nfs-utils
nilfs-utils
nitrogen
nm-connection-editor
nmap
noto-fonts
noto-fonts-emoji
ntfs-3g
nvme-cli
obconf
obmenu-generator
openbox
openconnect
openssh
openvpn
os-prober
p7zip
pamac-aur
partclone
parted
partimage
patch
pavucontrol
pcmanfm
pcsclite
perl-linux-desktopfiles
picom-ibhagwan-git
pkgconf
plank
plymouth
polkit
polybar
powertop
ppp
pptpclient
pulseaudio
pulseaudio-alsa
pulseaudio-bluetooth
pulseaudio-equalizer-ladspa
pv
python-pywal
python2
qt5ct
ranger
reflector
reiserfsprogs
rofi
rp-pppoe
rsync
rxvt-unicode-terminfo
screen
sddm
sdparm
sg3_utils
slop
smartmontools
sof-firmware
squashfs-tools
sshfs
sudo
sxhkd
systemd-resolvconf
tcpdump
terminus-font
thunar
thunar-archive-plugin
thunar-media-tags-plugin
thunar-volman
timeshift
tmux
toilet
tpm2-tss
trash-cli
ttf-dejavu
tty-clock
tumbler
udftools
udisks2
ueberzug
unimatrix-git
unrar
unzip
usb_modeswitch
usbmuxd
usbutils
viewnior
vim
vpnc
wget
wireless-regdb
wireless_tools
wmctrl
wpa_supplicant
wvdial
xarchiver
xclip
xdg-user-dirs
xdg-user-dirs-gtk
xdotool
xf86-input-libinput
xf86-video-fbdev
xf86-video-intel
xf86-video-vesa
xfce-polkit
xfce4-power-manager
xfce4-settings
xfce4-terminal
xfsprogs
xl2tpd
xmlstarlet
xorg-fonts-misc
xorg-server
xorg-xbacklight
xorg-xev
xorg-xfd
xorg-xinput
xorg-xkill
xorg-xmodmap
xorg-xrandr
xorg-xrdb
xorg-xset
xorg-xsetroot
xorg-xwininfo
xsettingsd
yad
yay
zip
zsh
